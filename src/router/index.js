import Vue from 'vue'
import Router from 'vue-router'

const apps = () => import('@/components/apps')
const sales = () => import('@/components/sales')
const purchasing = () => import('@/components/purchasing')
const Dashboard = () => import('@/components/MenuComponent/dashboard')
// const Sales = () => import('@/components/MenuComponent/Sales')
// const Purchasing = () => import('@/components/MenuComponent/purchasing')
const Inventory = () => import('@/components/MenuComponent/inventory')
const Accounting = () => import('@/components/MenuComponent/accounting')
const Reports = () => import('@/components/MenuComponent/reports')
// const Apps = () => import('@/components/MenuComponent/apps')

Vue.use(Router)

export default new Router({
  mode: 'history', // to remove # sign
  routes: [
    {
      path: '/',
      name: 'dashboard',
      component: Dashboard
    },
    {
      path: '/Reports',
      name: 'reports',
      component: Reports
    },
    {
      path: '/Inventory',
      name: 'inventory',
      component: Inventory
    },
    {
      path: '/Accounting',
      name: 'accounting',
      component: Accounting
    },
    {
      path: '/apps',
      name: 'apps',
      component: apps
    },
    {
      path: '/sales',
      name: 'sales',
      component: sales
    },
    {
      path: '/purchasing',
      name: 'purchasing',
      component: purchasing
    }
  ]
})
