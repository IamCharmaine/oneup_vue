// component
import Navigation from './navigation'
import Toolbar from './toolbar'
import apps from './apps'
import sales from './sales'
import Purchasing2 from './Purchasing2'

// tab components
import Accounting from './MenuComponent/ReportTabComponent/accounting'
import Sales from './MenuComponent/ReportTabComponent/sales'
import Purchasing from './MenuComponent/ReportTabComponent/purchasing'
import Logistics from './MenuComponent/ReportTabComponent/logistics'

// tabs for inventory
import DeliveryNotes from './MenuComponent/inventab/delivNotes'
import PurchaseOrder from './MenuComponent/inventab/purchaseOrder'
import SalesOrder from './MenuComponent/inventab/salesOrder'
import Stock from './MenuComponent/inventab/stock'

// tabs for accounting
import Record from './MenuComponent/acctabs/record'

// charts
import PieChart from '@/components/Charts/PieChart'
import LineChart from './Charts/LineChart'
import BarChart from './Charts/BarChart'
import PolarChart from './Charts/PolarChart'

export default {
  apps,
  sales,
  Navigation,
  Toolbar,
  Accounting,
  Sales,
  Purchasing,
  Purchasing2,
  Logistics,
  PieChart,
  LineChart,
  BarChart,
  PolarChart,
  DeliveryNotes,
  PurchaseOrder,
  SalesOrder,
  Record,
  Stock
}
