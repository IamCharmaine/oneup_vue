// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import Components from '@/components/index'
import Spinner from 'vue-spinkit'
import VueMoment from 'vue-moment'
import moment from 'moment-timezone'

import {
  Vuetify,
  VApp,
  VCard,
  VCheckbox,
  VDataIterator,
  VDataTable,
  VDialog,
  VDivider,
  VDatePicker,
  VJumbotron,
  VMenu,
  VNavigationDrawer,
  VFooter,
  VList,
  VBtn,
  VIcon,
  VGrid,
  VSelect,
  VTabs,
  VTextField,
  VToolbar,
  VTooltip,
  VTimePicker,
  transitions
} from 'vuetify'
import '../node_modules/vuetify/src/stylus/app.styl'

Vue.use(Vuetify, {
  components: {
    VApp,
    VCard,
    VCheckbox,
    VDataIterator,
    VDataTable,
    VDialog,
    VDivider,
    VDatePicker,
    VJumbotron,
    VMenu,
    VNavigationDrawer,
    VFooter,
    VList,
    VBtn,
    VIcon,
    VGrid,
    VSelect,
    VTabs,
    VTextField,
    VToolbar,
    VTooltip,
    VTimePicker,
    transitions
  },
  theme: {
    primary: '#ee44aa',
    secondary: '#424242',
    accent: '#82B1FF',
    error: '#FF5252',
    info: '#2196F3',
    success: '#4CAF50',
    warning: '#FFC107'
  }
})
Vue.use(VueMoment, {
  moment
})

Vue.component('Spinner', Spinner)

Object.keys(Components).forEach(key => {
  Vue.component(key, Components[key])
})

Vue.config.productionTip = false

Vue.prototype.$eventHub = new Vue()  // Global event bus
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
